# Tutoriais Instalação Banco de Dados



## Instalação do Ubuntu

* Baixe a imagem ... 
* Crie a VM (para funcionar na rede do Senai desabilite a rede)
* Configure a rede como NAT e proceda com a instalação.

Após concluir a instalação, habilite a placa de rede.

Suba a imagem e logue (com usuario e senha que vc configurou)

Edite o arquivo: 

```bash
nano /etc/netplan/00-installer-config.yaml
```

E coloque o seguinte conteúdo

```yaml
network:
  ethernets:
    enp0s3:
      dhcp4: true
  version: 2
```

Reinicie a máquina

## Instalação e configuração do postgres

Instale o postgres com o seguinte comando:

```bash
sudo apt install postgresql
```

Depois da instalação, é necessário configurar as seguintes portas NAT: 

![Configuração](imagens/nat_1.png "Configuração")

![Configuração](imagens/nat_2.png "Configuração")

### Configurações gerais: Fazendo o postgres escutar qualquer IP

Edite o arquivo:

```
nano /etc/postgresql/14/main/postgresql.conf
```

Localize a linha 

```
#listen_addresses = 'localhost'    # what IP address(es) to listen on;
```

e deixe da seguinte forma:

```
listen_addresses = '*'    # what IP address(es) to listen on;
```

* tire o comentário
* mude localhost para '*'

### Configurações gerais: Fazendo qualquer IP conectar no postgres

Edite o arquivo

```
nano /etc/postgresql/14/main/pg_hba.conf
```

E edite o conteúdo de 

```
host    all             all             127.0.0.21/32               scram-sha-256
```

Para

```
host    all             all             0.0.0.0/0               scram-sha-256
```

Reinicie o postgres:

```
service postgresql restart
```

### Configurações gerais: Modificando a senha do postgres

No prompt execute: 

```
sudo -u postgres psql
```

Depois

```SQL
ALTER USER postgres PASSWORD '123456';
```

## Conectando ao postgres


![Configuração](imagens/pg_01.png "Configuração")

